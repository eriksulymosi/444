<!-- ez nem lesz szép -->
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style>
        body {
            background: #f1f1f1;
            font-family: Arial, sans;
        }
        article {
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 3px 0 0 rgba(0,0,0,.2);
            background: #fff;
            margin-bottom: 20px;
        }
        .wrapper {
            width: 100%;
            max-width: 750px;
            margin: 20px auto;
        }

        button {
            border: 0;
            width: 100%;
            border-radius: 5px;
            padding: 15px 25px;
            font-size: 22px;
            text-decoration: none;
            margin: 20px 0;
            color: #fff;
            position: relative;
            display: inline-block;
            background-color: #e74c3c;
            box-shadow: 0px 5px 0px 0px #CE3323;
        }

        button:active {
            transform: translate(0px, 5px);
            -webkit-transform: translate(0px, 5px);
            box-shadow: 0px 1px 0px 0px;
        }
        button:hover {
            background-color: #FF6656;
        }

        button[disabled] {
            opacity: .5;
        }

        h1 {
            margin: 0;
            padding: 0 0 10px;
        }

        h1 small {
            font-weight: normal;
            padding-left: 10px;
            opacity: .7;
            font-size: 50%;
        }
        h1 small:before {
            content: 'by ';
        }

        footer {
            text-align: right;
            font-style: italic;
            font-size: 11px;
        }

        .half {
            display: flex;
            margin: 0 -15px;
        }

        .half button {
            margin: 0 15px;
        }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <button id="new">Create new article</button>
            <div id="posts-wrapper">

            </div>
            <div class="half">
                <button id="prev">Newer</button>
                <button id="next">Older</button>
            </div>
        </div>
        <script>
            class Negy {
                constructor(wrapperId = 'posts-wrapper') {
                    this.hashVariables = {}
                    this.parseHashVariables()
                    this.wrapper = document.getElementById(wrapperId)

                    this.showPage()

                    this.attachEventListeners();
                }

                // not a perfect nameing
                showPage() {
                    let page = this.getHashVariable('page', 1)

                    while (this.wrapper.firstChild) {
                        this.wrapper.removeChild(this.wrapper.firstChild);
                    }

                    this.getPosts(page).then((response) => {
                        if (! response.ok) {
                            throw Error(response.statusText);
                        }
                        return response;
                    }).then((response) => {
                        response.json().then((data) => {
                            this.updatePager(data.meta)
                            data.data.forEach((post) => {
                                this.renderPost(post)
                            })
                        })
                    }).catch((error) => {
                        // you can make it better
                        alert(error);
                        console.log(error);
                    })
                }

                getPosts(page = 1) {
                    return fetch(`/api/posts?page=${page}`, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8',
                        },
                    })
                }

                getPostDOM(post) {
                    let article = document.createElement('article')

                    let header = document.createElement('header')
                    let h1 = document.createElement('h1')
                    h1.append(document.createTextNode(post.title))
                    let small = document.createElement('small')
                    small.append(document.createTextNode(post.author))
                    h1.append(small)
                    header.append(h1)
                    article.append(header)

                    let body = document.createElement('p')
                    body.append(document.createTextNode(post.body))
                    article.append(body)

                    let footer = document.createElement('footer')
                    footer.append(document.createTextNode(post.created_at.date))
                    article.append(footer)

                    return article
                }

                renderPost(post, prepend = false) {
                    let dom = this.getPostDOM(post)

                    let action = prepend?'prepend':'append'

                    this.wrapper[action](dom);
                }

                updatePager(meta) {
                    let prev = document.getElementById('prev')
                    let next = document.getElementById('next')
                    prev.disabled = meta.current_page <= 1
                    next.disabled = meta.to >= meta.total
                }

                attachEventListeners() {
                    this.attachEventListener(window, 'hashchange', (ev) => {
                        this.hashVariables = {}
                        this.parseHashVariables()
                        this.showPage()
                    })

                    this.attachEventListener('#new', 'click', (ev) => {

                        fetch('/api/posts', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json; charset=utf-8',
                            },
                        }).then((response) =>  {
                            if (! response.ok) {
                                throw Error(response.statusText);
                            }
                            return response
                        }).then((response) => {
                            response.json().then((data) => {
                                this.renderPost(data, true)
                                this.setHashVariable('page', 1)
                                this.updateHash()
                            })
                        }).catch((error) => {
                            // you can make it better
                            alert(error);
                            console.log(error);
                        })
                    })

                    this.attachEventListener('#prev', 'click', (ev) => {
                        this.setHashVariable('page', this.getHashVariable('page', 2) - 1)
                        this.updateHash();
                    })
                    this.attachEventListener('#next', 'click', (ev) => {
                        this.setHashVariable('page', +this.getHashVariable('page', 1) + 1)
                        this.updateHash();
                    })
                }

                attachEventListener(target, ev, cb) {
                    if (target instanceof EventTarget) {
                        target.addEventListener(ev, cb)
                        return
                    }
                    let elements = document.querySelectorAll(target)
                    elements.forEach((el) => {
                        el.addEventListener(ev, cb)
                    })
                }

                getHashVariable(variable, def = null) {
                    return this.hashVariables[variable] || def
                }

                setHashVariable(key, value) {
                    this.hashVariables[key] = value
                }

                parseHashVariables() {
                    let query = location.hash.substring(1)
                    let vars = query.split('&')

                    vars.forEach((variable) => {
                        let pair = variable.split('=');
                        this.hashVariables[decodeURIComponent(pair[0])] = pair[1] !== undefined ? decodeURIComponent(pair[1]) : null
                    })
                }

                updateHash() {
                    let hashString = '#'
                    for (var key in this.hashVariables) {
                        if (this.hashVariables.hasOwnProperty(key)) {
                            hashString += key + (this.hashVariables[key] !== null ? '=' + this.hashVariables[key] : '')
                        }
                    }

                    location.hash = hashString
                }
            };

            let app = new Negy()
        </script>
    </body>
</html>
