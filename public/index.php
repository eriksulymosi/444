<?php

require __DIR__.'/../vendor/autoload.php';

$app = new Negy\App(
    realpath(__DIR__.'/../')
);

$app->run();
