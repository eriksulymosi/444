<?php

$router->add('get', '', function () {
    return new Negy\View('list');
});

$router->add('get', 'api/posts', 'PostsController@index');
$router->add('post', 'api/posts', 'PostsController@store');
$router->add('get', 'api\/posts\/([0-9a-z]*)', 'PostsController@show');
$router->add('post', 'api\/posts\/([0-9a-z]*)', 'PostsController@update');
$router->add('post', 'api\/posts\/([0-9a-z]*)/delete', 'PostsController@delete');
