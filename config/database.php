<?php

return [
    'connection' => 'mongo',

    'connections' => [
        'mongo' => [
            'driver'   => Negy\DB\Drivers\MongoDB\MongoDB::class,
            'host'     => 'mongo',
            'port'     => '27017',
            'database' => 'database',
            'username' => '',
            'password' => '',
            'options'  => []
        ]
    ]
];
