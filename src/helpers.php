<?php

if (! function_exists('array_get')) {
    function array_get($array, $key, $default = null)
    {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }
        if (strpos($key, '.') === false) {
            return $default;
        }
        foreach (explode('.', $key) as $segment) {
            if (!is_array($array) || !array_key_exists($segment, $array)) {
                return $default;
            }
            $array = &$array[$segment];
        }
        return $array;
    }
}

if (! function_exists('array_set')) {
    function array_set(&$array, $keys, $value = null)
    {
        if (is_array($keys)) {
            foreach ($keys as $key => $value) {
                array_set($key, $value);
            }
            return;
        }
        foreach (explode('.', $keys) as $key) {
            if (!isset($array[$key]) || !is_array($array[$key])) {
                $array[$key] = [];
            }
            $array = &$array[$key];
        }
        $array = $value;
    }
}

if (! function_exists('is_assoc')) {
    function is_assoc(array $arr)
    {
        return empty($arr) ? false : array_keys($arr) !== range(0, count($arr) - 1);
    }
}

if (! function_exists('abs_realpath')) {
    function abs_realpath($path)
    {
        $path = explode('/', $path);
        $keys = array_keys($path, '..');

        foreach ($keys as $keypos => $key) {
            array_splice($path, $key - ($keypos * 2 + 1), 2);
        }

        $path = implode('/', $path);
        return str_replace('./', '', $path);
    }
}

if (! function_exists('app')) {
    function app()
    {
        return Negy\App::getInstance();
    }
}

if (! function_exists('str_studly')) {
    function str_studly($value)
    {
        $value = ucwords(str_replace(['-', '_'], ' ', $value));
        return str_replace(' ', '', $value);
    }
}
