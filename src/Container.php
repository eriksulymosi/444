<?php

namespace Negy;

use Closure;
use Exception;

class Container
{
    protected $services = [];
    protected $shared = [];

    public function add($service, Closure $closure)
    {
        $this->services[$service] = $closure;
        return $this;
    }

    public function has($service)
    {
        return array_key_exists($service, $this->services);
    }

    public function get($service)
    {
        if (! array_key_exists($service, $this->shared)) {
            $this->shared[$service] = $this->getNew($service);
        }

        return $this->shared[$service];
    }

    public function getNew($service)
    {
        if (! $this->has($service)) {
            throw new Exception("Service '$service' not found");
        }

        $container = $this->services[$service];

        return $container();
    }
}
