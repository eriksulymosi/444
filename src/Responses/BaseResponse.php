<?php

namespace Negy\Responses;

class BaseResponse
{
    protected $headers = [];
    protected $statusCode = 200;
    protected $body;

    public function __construct($body, int $statusCode = 200, array $headers = [])
    {
        $this->headers = $headers;
        $this->statusCode = $statusCode;
        $this->body = $body;
    }

    public function __toString()
    {
        $this->printHeaders();

        return $this->render();
    }

    public function render()
    {
        return $this->body;
    }

    public function setHeader(string $name, string $value = '')
    {
        $this->headers[$name] = $value;
        return $this;
    }

    public function getHeader(string $name = null)
    {
        return array_get($this->header, $name, $this->header);
    }

    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    protected function printHeaders()
    {
        foreach ($this->headers as $key => $value) {
            header("{$key}: {$value}");
        }

        http_response_code($this->statusCode);
    }
}
