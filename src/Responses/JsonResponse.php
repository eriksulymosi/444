<?php

namespace Negy\Responses;

class JsonResponse extends BaseResponse
{
    public function __construct($body, int $statusCode = 200, array $headers = [])
    {
        parent::__construct($body, $statusCode, $headers);
        $this->setHeader('Content-Type', 'application/json');
    }

    public function render()
    {
        return json_encode($this->body);
    }
}
