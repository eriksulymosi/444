<?php

namespace Negy\Responses;

class HtmlResponse extends BaseResponse
{
    public function __construct($body, int $statusCode = 200, array $headers = [])
    {
        parent::__construct($body, $statusCode, $headers);
        $this->setHeader('Content-Type', 'text/html');
    }

    public function render()
    {
        return $this->body->__toString();
    }
}
