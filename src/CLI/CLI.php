<?php

namespace Negy\CLI;

use Negy\App;

class CLI
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function run()
    {
        global $argv;
        $args = array_slice($argv, 1);

        $taskClassName = 'Negy\\CLI\\Tasks\\' . str_studly($args[0]);

        if (class_exists($taskClassName)) {
            $handler = new $taskClassName;

            $handler->handle(array_slice($args, 1));
        }
    }
}
