<?php

namespace Negy\CLI\Tasks;

use MongoDB\Driver\Command;

class Migrate
{
    public function handle()
    {
        $mongo = $this->getMongo();

        if (! $this->isCollectionExists($mongo, 'posts')) {
            $this->createCollection($mongo, 'posts');
            echo "Posts collection migrated." . PHP_EOL;
        }
    }

    protected function getMongo()
    {
        return app()->db;
    }

    protected function isCollectionExists($mongo, string $collectionName)
    {
        $connection = $mongo->getConnection();
        $cmd = new Command(['listCollections' => 1, 'filter' => ['name' => $collectionName], 'nameOnly' => true]);
        $res = $connection->executeCommand($mongo->getConfig('database'), $cmd);
        return ! empty($res->toArray());
    }

    protected function createCollection($mongo, string $collectionName, array $options = [])
    {
        $connection = $mongo->getConnection();

        $cmd = new Command(array_merge([
            'create' => $collectionName,
            'capped' => true,
            'size' => 64 * 1024,
            'autoIndexId' => true
        ], $options));

        return $connection->executeCommand($mongo->getConfig('database'), $cmd);
    }
}
