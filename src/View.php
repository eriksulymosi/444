<?php

namespace Negy;

class View
{
    protected $view;
    protected $data = [];

    public function __construct($view, $data = [])
    {
        $this->view = $view;
        $this->data = $data;
    }

    public function __toString()
    {
        return $this->loadView();
    }

    public function with($key, $value = null)
    {
        if (is_array($key)) {
            foreach ($key as $k => $v) {
                $this->with($k, $v);
            }
        }

        if (is_string($key)) {
            array_set($key, $value);
        }

        return $this;
    }

    protected function loadView()
    {
        ob_start();
        app()->loadFile("views/{$this->view}.php", $this->data);
        return ob_get_clean();
    }
}
