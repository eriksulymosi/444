<?php

namespace Negy;

use Negy\App;
use Throwable;

class Notifier
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function notif(Throwable $throwable)
    {
        // Ezt azért szebben is lehetne
        $addresses = $this->app->config->get('notifier.email_on_error');
        foreach ($addresses as $name => $email) {
            mail("{$name} <{$email}>", 'Error volt!', json_encode($throwable));
        }
    }
}
