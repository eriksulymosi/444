<?php

namespace Negy\DB\Drivers\MongoDB;

use MongoDB\Driver\Manager;

class MongoDB
{
    protected $connection;
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;

        $dsn = $this->getHostDsn($config);
        $options = array_get($config, 'options', []);

        $this->connection = $this->createConnection($dsn, $config, $options);
    }

    public function __call(string $name, array $parmas = [])
    {
        $queryBuilder = new QueryBuilder($this->connection, array_get($this->config, 'database'));

        return $queryBuilder->$name(...$parmas);
    }

    public function getConfig($key = null)
    {
        return array_get($this->config, $key);
    }

    public function getConnection()
    {
        return $this->connection;
    }

    protected function createConnection($dsn, array $config, array $options)
    {
        $driverOptions = [];
        if (isset($config['driver_options']) && is_array($config['driver_options'])) {
            $driverOptions = $config['driver_options'];
        }

        if (!isset($options['username']) && !empty($config['username'])) {
            $options['username'] = $config['username'];
        }
        if (!isset($options['password']) && !empty($config['password'])) {
            $options['password'] = $config['password'];
        }
        return new Manager($dsn, $options, $driverOptions);
    }

    protected function getHostDsn(array $config)
    {
        $hosts = is_array($config['host']) ? $config['host'] : [$config['host']];
        foreach ($hosts as &$host) {
            if (strpos($host, ':') === false && !empty($config['port'])) {
                $host = $host . ':' . $config['port'];
            }
        }
        $authDatabase = isset($config['options']) && !empty($config['options']['database']) ? $config['options']['database'] : null;
        return 'mongodb://' . implode(',', $hosts) . ($authDatabase ? '/' . $authDatabase : '');
    }
}
