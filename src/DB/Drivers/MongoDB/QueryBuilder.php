<?php

namespace Negy\DB\Drivers\MongoDB;

use MongoDB\Driver\Query;
use MongoDB\Driver\Command;
use MongoDB\Driver\BulkWrite;

class QueryBuilder
{
    protected $connection;
    protected $database;

    public function __construct($connection, $database)
    {
        $this->connection = $connection;
        $this->database = $database;
    }

    public function command($document, array $commandOptions = [])
    {
        $command = new Command($document, $commandOptions);

        return $this->connection->executeCommand($this->database, $command);
    }

    public function query(string $collection, array $filter = [], array $options = [])
    {
        $query = new Query($filter, $options);

        return $this->connection->executeQuery(
            "{$this->database}.{$collection}",
            $query
        );
    }

    public function insert(string $collection, array $data, array $bulkWriteOptions = [], array $executeOptions = [])
    {
        $bulkWrite = new BulkWrite($bulkWriteOptions);

        $this->insertToWrite($bulkWrite, $data);

        return $this->connection->executeBulkWrite(
            "{$this->database}.{$collection}",
            $bulkWrite,
            $executeOptions
        );
    }

    public function update(string $collection, array $data, array $bulkWriteOptions = [], array $executeOptions = [])
    {
        $bulkWrite = new BulkWrite($bulkWriteOptions);

        $this->updateToWrite($bulkWrite, $data);

        return $this->connection->executeBulkWrite(
            "{$this->database}.{$collection}",
            $bulkWrite,
            $executeOptions
        );
    }

    public function delete(string $collection, array $data, array $bulkWriteOptions = [], array $executeOptions = [])
    {
        $bulkWrite = new BulkWrite($bulkWriteOptions);

        $this->deleteToWrite($bulkWrite, $data);

        return $this->connection->executeBulkWrite(
            "{$this->database}.{$collection}",
            $bulkWrite,
            $executeOptions
        );
    }

    protected function insertToWrite($write, $data)
    {
        if (is_assoc($data)) {
            return $write->insert($data);
        }

        foreach ($data as $value) {
            $this->insertToWrite($write, $value);
        }
    }

    protected function updateToWrite($write, $data)
    {
        if (isset($data[1]['$set']) && count($data) === 2) {
            return $write->update(...$data);
        }

        foreach ($data as $value) {
            $this->updateToWrite($write, $data);
        }
    }

    protected function deleteToWrite($write, $data)
    {
        if (is_assoc($data)) {
            return $write->delete($data);
        }

        foreach ($data as $value) {
            $this->deleteToWrite($write, $value);
        }
    }
}
