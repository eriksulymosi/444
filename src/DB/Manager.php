<?php

namespace Negy\DB;

use Negy\App;
use Negy\Container;

class Manager extends Container
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;

        $this->prepareConnections();
    }

    public function __call(string $name, array $parmas = [])
    {
        return $this->default->$name(...$parmas);
    }

    public function __get(string $name)
    {
        if ($name === 'default') {
            $name = $this->app->config->get('database.connection');
        }
        return $this->get($name);
    }

    protected function prepareConnections()
    {
        $connections = $this->app->config->get('database.connections');

        foreach ($connections as $name => $connection) {
            $this->add($name, function () use ($connection) {
                return new $connection['driver']($connection);
            });
        }
    }
}
