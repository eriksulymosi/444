<?php

namespace Negy;

use Throwable;
use Negy\View;
use Negy\Logger\LogLevel;
use Negy\Responses\JsonResponse;
use Negy\Responses\HtmlResponse;
use Negy\Exceptions\HttpException;

class ErrorHandler
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
        set_exception_handler([$this, 'handleException']);
    }

    public function handleException(Throwable $throwable)
    {
        $statusCode = $throwable instanceof HttpException?$throwable->getCode():500;

        if ($this->app->has('logger')) {

            $this->app->logger->log(LogLevel::ERROR, $throwable->getMessage());
        }

        if ($this->app->has('notifier')) {
            $this->app->notifier->notif($throwable);
        }

        if ($this->app->isAjax() || true) {
            echo new JsonResponse($this->throwableToArray($throwable), $statusCode);
            return;
        }

        echo new HtmlResponse(
            new View(
                $this->app->isDebug()?'error/debug':'error/production',
                [
                    'error' => $throwable
                ]
            ),
            $statusCode
        );
    }

    protected function throwableToArray(Throwable $throwable)
    {
        if (! $this->app->isDebug()) {
            return [
                'errors' => 'Sorry, something went wrong.'
            ];
        }

        return [
            'exception' => get_class($throwable),
            'message' => $throwable->getMessage(),
            'trace' => $throwable->getTrace()
        ];
    }
}
