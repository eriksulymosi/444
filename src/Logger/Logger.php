<?php

namespace Negy\Logger;

use Negy\App;

class Logger
{
    protected $app;

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function log($level, $message, array $context = [])
    {
        error_log("[{$level}] {$message}");
    }
}
