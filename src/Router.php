<?php

namespace Negy;

use Closure;
use Negy\Exceptions\NotFoundException;
use Negy\Exceptions\MethodNotAllowedException;

class Router
{
    private $routes = [];

    public function add($method, $pattern, $callback)
    {
        $method = strtolower($method);
        $pattern = trim($pattern, '/');

        $pattern = '~^' . $pattern . '$~';
        $this->routes[$pattern][$method] = $callback;
    }

    public function exec($method, $url)
    {
        $method = strtolower($method);
        $url = trim($url, '/');

        foreach ($this->routes as $pattern => $route) {
            if (preg_match($pattern, $url, $params)) {
                if ($this->hasHandler($route, $method)) {
                    $rawHandler = $this->getHandler($route, $method);
                    $handler = $this->resolveHandler($rawHandler);

                    array_shift($params);

                    if ($handler !== null) {
                        return call_user_func_array($handler, $params);
                    }
                }
                throw new MethodNotAllowedException();
            }
        }

        throw new NotFoundException();
    }

    protected function resolveHandler($callback)
    {
        if (is_callable($callback)) {
            return $callback;
        }

        if (is_string($callback) && strpos($callback, '@')) {
            [$controller, $method] = explode('@', 'Negy\\Http\\Controllers\\' . $callback);

            return Closure::fromCallable([new $controller, $method]);
        }
    }

    protected function hasHandler($route, $method)
    {
        return isset($route[$method]) || isset($route['any']);
    }

    protected function getHandler($route, $method)
    {
        return $route[$method] ?? $route['any'];
    }
}
