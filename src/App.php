<?php

namespace Negy;

use Closure;
use ArrayAccess;
use Faker\Factory;
use Negy\Responses\BaseResponse;
use Negy\Responses\HtmlResponse;
use Negy\Responses\JsonResponse;

class App extends Container
{
    protected static $instance;
    protected $basePath;

    public function __construct(string $basePath)
    {
        self::$instance = $this;

        $this->basePath = $basePath;

        $this->addCoreServices();
        $this->bootstrap();
    }

    public function __get(string $name)
    {
        return $this->get($name);
    }

    public function getInstance()
    {
        return self::$instance;
    }

    public function run()
    {
        $questionMark = strpos($_SERVER['REQUEST_URI'], '?');
        $response = $this->router->exec(
            $_SERVER['REQUEST_METHOD'],
            substr($_SERVER['REQUEST_URI'], 0, $questionMark !== false ? $questionMark : strlen($_SERVER['REQUEST_URI']))
        );

        echo $this->processResponse($response);
    }

    public function processResponse($response)
    {
        if ($response instanceof BaseResponse) {
            return $response;
        }

        if (is_array($response) || $response instanceof ArrayAccess) {
            return new JsonResponse($response);
        }

        if (is_string($response) || $response instanceof View) {
            return new HtmlResponse($response);
        }

        return new BaseResponse($response);
    }

    public function path($additional = '')
    {
        return abs_realpath($this->basePath . '/' . $additional);
    }

    public function loadFile($path, $params = [], $scope = null)
    {
        $loader = function ($routesPath) use ($params) {
            extract($params);
            return require_once $routesPath;
        };
        $loader = $loader->bindTo($scope);
        return $loader($this->path($path));
    }

    public function isAjax()
    {
        return ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    public function isDebug()
    {
        return $this->config->get('app.debug', false);
    }

    protected function bootstrap()
    {
        $this->get('errorHander');
    }

    protected function addCoreServices()
    {
        $this->add('config', function () {
            return new Config($this);
        });

        $this->add('errorHander', function () {
            return new ErrorHandler($this);
        });
        $this->add('db', function () {
            return new DB\Manager($this);
        });

        $this->add('router', function () {
            $router = new Router($this);
            $this->loadFile('routes.php', ['router' => $router]);
            return $router;
        });

        $this->add('faker', function () {
            return Factory::create();
        });

        if (php_sapi_name() == "cli") {
            $this->add('cli', function () {
                return new CLI\CLI($this);
            });
        }

        $this->add('logger', function () {
            return new Logger\Logger($this);
        });

        $this->add('notifier', function () {
            return new Notifier($this);
        });
    }
}
