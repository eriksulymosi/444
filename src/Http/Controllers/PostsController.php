<?php

namespace Negy\Http\Controllers;

use DateTime;
use MongoDB\BSON\ObjectId;

class PostsController
{
    public function index()
    {
        $total = app()->db->command([
            'count' => 'posts'
        ]);

        $arr = $total->toArray();
        $totalRows = array_pop($arr)->n;

        $page = max(0, min(array_get($_GET, 'page', 1) - 1, floor($totalRows / 15)));

        $posts = app()->db->query('posts', [], [
            'limit' => 15,
            'skip' => $page * 15,
            'sort' => ['_id' => -1]
        ]);

        $items = $posts->toArray();

        return [
            'data' => $items,
            'meta' => [
                'current_page' => min($page, floor($totalRows / 15)) + 1,
                'from' => empty($items)?0:$page * 15 + 1,
                'to' => empty($items)?0:($page * 15) + count($items),
                'total' => $totalRows
            ]
        ];
    }

    public function store()
    {
        // Random error
        $dobokocka = rand(1, 6);

        if ($dobokocka < 3) {
            throw new \Exception('Random ledöglött ez a szar. :(');
        }

        // End Random error

        $data = [
            '_id' => new ObjectId,
            'title' => app()->faker->sentence,
            'author' => app()->faker->name,
            'body' => app()->faker->text(400),
            'created_at' => new DateTime()
        ];

        $post = app()->db->insert('posts', $data);

        return $data;
    }

    public function show($id)
    {
        $post = app()->db->query('posts', [
            '_id' => new ObjectId($id)
        ]);
        $arr = $post->toArray();

        return (array) array_pop($arr);
    }

    public function update($id)
    {
        // TODO ;)
    }

    public function delete($id)
    {
        // TODO ;)
    }
}
