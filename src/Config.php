<?php

namespace Negy;

class Config
{
    protected $app;
    protected $configs = [];

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    public function get($key, $default = null)
    {
        $fileName = substr($key, 0, strpos($key, '.'));
        if (! $this->isFileLoaded($fileName)) {
            array_set(
                $this->configs,
                $fileName,
                $this->app->loadFile("config/{$fileName}.php")
            );
        }
        return array_get($this->configs, $key, $default);
    }

    protected function isFileLoaded(string $fileName)
    {
        return isset($this->configs[$fileName]);
    }
}
