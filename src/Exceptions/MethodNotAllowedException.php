<?php

namespace Negy\Exceptions;

use Exception;

class MethodNotAllowedException extends HttpException
{
    public function __construct(string $message = '', int $code = 405, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
